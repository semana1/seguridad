package com.techu.seguridad.config;

import java.io.Serializable;
import java.util.function.Function;

public class JwtTokenUtil implements Serializable {

    public static final long JWT_TOKEN_VALIDITY = 5*60*60;

    @value("${jwt.secret}")
    private String secreto;

    public <T> T getClaimFromToken(String token, Function<Claims, T claimsResolver>)
    final Claims claims = getAllClaimsFromToken(String token){
        return Jwts.parser().setSigninKey(secreto).parseClaimsJws(token).getBody();
    }
    public String getUsernameFromToken(String)


}
